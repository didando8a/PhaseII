<?php

require_once __DIR__ . '/' . 'vendor/autoload.php';
include_once __DIR__ . '/src/Daniel/Setup/Doctrine.php';

if ($argc != 2) {
    echo "Usage: requestDataFromUrl.php {'url'}";
    exit();
}
//echo $argv[1];exit();

$app = new \Test\Business\DataManager($argv[1], $entityManager);

$app
    ->makeRequest()
    ->parseDataToXml()
    ->showInfo()
;