<?php
use PHPUnit\Framework\TestCase;

class SumTest extends TestCase
{
    public function testPushAndPop()
    {
        $first = 1;
        $second = 1;
        $this->assertEquals(first + second, 2);
    }
}