<?php

namespace Test\Business;

use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Test\Model\Result;

class DataManager
{
    protected $url;

    protected $xmlDataRetrieved;

    protected $simpleXmlObject;

    protected $entityManager;

    public function __construct($url, EntityManager $entityManager)
    {
        $this->url = $url;
        $this->entityManager = $entityManager;
    }

    public function makeRequest()
    {
        $client = new Client();
        $response = $client->get($this->url);
        $this->xmlDataRetrieved = $response->xml();

        return $this;
    }

    public function parseDataToXml()
    {
        $this->simpleXmlObject = simplexml_import_dom($this->xmlDataRetrieved);

        return $this;
    }

    public function showInfo()
    {
        echo sprintf("============================\n");
        echo sprintf("\tINFO MAPPED\n");
        echo sprintf("============================\n\n");
        $headerMetadata = true;

        if (count($this->simpleXmlObject) == 0 || count($this->simpleXmlObject) == 1) {
            echo "No info retrieved";
        } else {
            foreach ($this->simpleXmlObject as $key => $element) {
                if ($headerMetadata) {
                    $headerMetadata = false;
                } else {
                    echo sprintf("NCT ID: %s\n", $element->nct_id);
                    echo sprintf("Title: %s\n", $element->title);
                    echo sprintf("Status: %s\n", $element->status);
                    $datetime = new \DateTime($element->last_changed);
                    echo sprintf("Last changed: %s\n", $datetime->format('d/m/Y'));
                    echo sprintf("============================\n");
                    $result = new Result($element->nct_id, $element->title, $element->status, $datetime);
                    /**
                     * @internal Comment to just print
                     */
                    $this->entityManager->persist($result);
                    unset($datetime);
                }
            }
            /**
             * @internal Comment to just print
             */
            $this->entityManager->flush();
        }
    }
}
