<?php

namespace Test\Model;


class Result {
    /**
     * @Column(type="integer", name="nct_id", nullable=false)
     */
    protected $NCTID;

    /**
     * @Column(name="title", type="string")
     */
    protected $title;

    /**
     * @Column(type="string", name="status", columnDefinition="ENUM('Completed','Recruiting','Active, not recruiting', 'Withdrawn')")
     */
    protected $status;

    /**
     * @Column(type="datetime", name="last_changed", nullable=false)
     */
    protected $lastChanged;

    public function __construct($NCTID, $title, $status, $lastChanged)
    {
        $this->NCTID = $NCTID;
        $this->title = $title;
        $this->status = $status;
        $this->lastChanged = $lastChanged;
    }


    public function getNCTID()
    {
        return $this->NCTID;
    }

    /**
     * @param $NCTID
     * @return $this
     */
    public function setNCTID($NCTID)
    {
        $this->NCTID = $NCTID;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }


    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getLastChanged()
    {
        return $this->lastChanged;
    }


    /**
     * @param $lastChanged
     * @return $this
     */
    public function setLastChanged($lastChanged)
    {
        $this->lastChanged = $lastChanged;

        return $this;
    }

}
